const times = document.getElementsByClassName("time");
const content = document.getElementById("content");
const timesArray = Array.prototype.slice.call(times);

const classNames = {
  0: "grid-rows-[2fr_0.5fr_0.5fr_0.5fr]",
  1: "grid-rows-[0.5fr_2fr_0.5fr_0.5fr]",
  2: "grid-rows-[0.5fr_0.5fr_2fr_0.5fr]",
  3: "grid-rows-[0.5fr_0.5fr_0.5fr_1fr]",
};
content.addEventListener("mouseout", () => {
  content.classList.add("grid-rows-[2fr_0.5fr_0.5fr_0.5fr]");
});
timesArray.forEach((time, activeIndex) => {
  time.addEventListener("mouseenter", () => {
    content.classList.add(classNames[activeIndex]);
    if (activeIndex !== 0) content.classList.remove(classNames[0]);
  });
  time.addEventListener("mouseout", () => {
    content.classList.remove(classNames[activeIndex]);
  });
});
