/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./**/*.{html,js,tsx}"],
  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: "#02634c",
          50: "#ebfef6",
          100: "#cffce7",
          200: "#a3f7d4",
          300: "#68edbe",
          400: "#2cdba2",
          500: "#08c18c",
          600: "#009d73",
          700: "#007e5e",
          800: "#02634c",
          900: "#035140",
          950: "#003329",
        },
      },
      gridRowEnd: {
        8: "8",
        9: "9",
        10: "10",
        11: "11",
        12: "12",
        13: "13",
      },
      animation: {
        "slide-down": "slideDown 0.5s cubic-bezier(.165,.84,.4,1) infinite",
        "slide-up": "slideUp 0.5s cubic-bezier(.165,.84,.4,1)",
        "slide-in-left": "slideInLeft 1s cubic-bezier(.165,.84,.4,1)",
        "slide-in-right": "slideInRight 1.5s cubic-bezier(.165,.84,.4,1)",
        "open-circle": "openCircle 2s cubic-bezier(.165,.84,.4,1)",
        "scale-up-y": "scaleUpY 0.7s ease-in-out infinite",
        rotate: "rotate 2s linear infinite",
        "rain-drop": "rainDrop 1s linear infinite",
      },
      keyframes: {
        slideDown: {
          "0%": { transform: "translateY(-100%)" },
          "100%": { transform: "translateY(0)" },
        },
        slideUp: {
          "0%": { transform: "translateY(100%)" },
          "100%": { transform: "translateY(0)" },
        },
        slideInLeft: {
          "0%": { transform: "translateX(-100%)" },
          "100%": { transform: "translateY(0)" },
        },
        slideInRight: {
          "0%": { transform: "translateX(100%)" },
          "100%": { transform: "translateY(0)" },
        },
        openCircle: {
          "0%": { clipPath: "circle(0.0% at 50% 50%)" },
          "50%": { clipPath: "circle(30.0% at 50% 50%)" },
          "100%": { clipPath: "circle(100.0% at 50% 50%)" },
        },
        scaleUpY: {
          "0%": { gridRow: "span 1 / span 1" },
          "100%": { gridRow: "span 2 / span 2" },
        },
        rotate: {
          "0%": { transform: "rotate(0deg)" },
          "20%": { transform: "rotate(72deg)" },
          "40%": { transform: "rotate(144deg)" },
          "60%": { transform: "rotate(216deg)" },
          "80%": { transform: "rotate(288deg)" },
          "100%": { transform: "rotate(360deg)" },
        },
        rainDrop: {
          "0%": {
            transform: "translateY(0) translateX(0) skewX(-10deg)",
            opacity: "1.0",
          },
          // "20%": {
          //   transform: "translateY(20%) skewX(-10deg)",
          //   opacity: "0.8",
          // },
          // "60%": {
          //   transform: "translateY(60%) skewX(-10deg)",
          //   opacity: "0.6",
          // },
          // "80%": { transform: "translateY(80%) skewX(-10deg)", opacity: "0.2" },
          "100%": {
            transform: "translateY(100%) translateX(-10%) skewX(-10deg)",
            opacity: "0",
          },
        },
      },
      animationDelay: {
        250: "250ms",
        750: "750ms",
      },
    },
  },
  plugins: [require("tailwindcss-animation-delay")],
};
