document.addEventListener("scroll", (event) => {
  const value = document.scrollingElement.scrollTop;
  const title = document.getElementById("parallax-title");
  title.style.marginTop = value * 1.5 + "px";
  const leaf = document.getElementById("leaf");
  leaf.style.top = -value * 0.5 + "px";
  const leftHill = document.getElementById("left-hill");
  leftHill.style.left = -value * 0.5 + "px";
  const rightHill = document.getElementById("right-hill");
  rightHill.style.right = -value * 0.5 + "px";

  const imageRightElement = document.getElementById("image-right");
  const imageLeftElement = document.getElementById("image-left");

  if (isInViewport(imageRightElement)) {
    imageRightElement.style.display = "block";
    imageRightElement.className =
      "object-cover rounded h-[500px] w-full animate-slide-in-right";
  }
  if (isInViewport(imageLeftElement)) {
    imageLeftElement.style.opacity = "1";
    imageLeftElement.className =
      "object-cover rounded w-[640px] h-[480px] animate-open-circle";
  }
});
function isInViewport(element) {
  const rectangle = element.getBoundingClientRect();
  const top = rectangle.top;
  const left = rectangle.left;

  return (
    top >= 0 &&
    left >= 0 &&
    top < window.innerHeight &&
    left < window.innerWidth
  );
}
